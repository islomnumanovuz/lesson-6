const btns = document.querySelectorAll(".btn").length;
let body = document.querySelector("body");

for (let i = 0; i < btns; i++) {
  document.querySelectorAll(".btn")[i].addEventListener("click", function () {
    let buttonInnerHTML = this.innerHTML;
    switch (buttonInnerHTML) {
      case "1":
        body.style.background = "lightskyblue";
        break;
      case "2":
        body.style.background = "yellow";
        break;
      case "3":
        body.style.background = "red";
        break;
      case "4":
        body.style.background = "blue";
        break;
      case "5":
        body.style.background = "greenyellow";
        break;
      case "6":
        body.style.background = "purple";
        break;
      case "7":
        body.style.background = "black";
      default:
        break;
    }
  });
}
